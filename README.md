# EmbedSocial_Internship_Challenge

This is the solution for the Embed Social Internship Challenge. It is a web application that fetches data from a JSON file and dynamically generates cards to display the content. It also provides the option to load more cards and toggle a dark theme.

## Important

To run this project locally, you will need a web browser and a local development server.

### Usage

- The web page will display a list of posts fetched from the `data.json` file.
- The application is responsive and will adapt to different screen sizes, providing an optimal viewing experience on both desktop and mobile devices.
- Use the "Load More" button to fetch and display more posts.
- You can enable the dark theme by checking the "Dark Theme" checkbox.
- Click on the post images to view them in a modal window.
- Click the close button on the modal window to close it.

