fetch("data.json")
  .then((response) => response.json())
  .then((data) => {
    location.hash = 0;

    let cardsContainer = document.querySelector(".cards-container");
    let loadMoreBtn = document.querySelector("#loadMoreBtn");
    let checkbox = document.querySelector('input[type="checkbox"]');

    loadPosts();
    loadMoreBtn.addEventListener("click", loadPosts);
    loadMoreBtn.addEventListener("click", applyDarkTheme);
    checkbox.addEventListener("change", applyDarkTheme);

    function applyDarkTheme() {
      let body = document.querySelector("body");
      let cards = document.querySelectorAll(".card");
      let paragraphs = document.querySelectorAll("p");
      let links = document.querySelectorAll("a");

      let bodyBgColor = "";
      let cardBgColor = "";
      let paragraphColor = "";
      let linkColor = "";

      if (checkbox.checked) {
        bodyBgColor = "#111827";
        cardBgColor = "#374151";
        paragraphColor = "#FFF9FA";
        linkColor = "#6366f1";
      }

      body.style.backgroundColor = bodyBgColor;

      paragraphs.forEach((paragraph) => {
        paragraph.style.color = paragraphColor;
      });

      cards.forEach((card) => {
        card.style.backgroundColor = cardBgColor;
      });

      links.forEach((link) => {
        link.style.color = linkColor;
      });
    }

    function loadPosts() {
      const hashValue = location.hash;
      const number = hashValue.substring(1);

      const currentIndex = Number(number);
      const postsPerLoad = 4;
      const endIndex = (currentIndex + postsPerLoad) - 1;
      
      let count = 0;
      for (let i = currentIndex; i < data.length; i++) {
        count++;
        
        // creating elements
        let card = document.createElement("div");
        card.classList.add("card");

        // image and image modal
        let imageContainer = document.createElement("div");
        imageContainer.classList.add("image-container");

        let imageElement = document.createElement("img");
        imageElement.classList.add("image");
        imageElement.setAttribute("src", data[i].image);

        let imageModal = document.createElement("img");
        imageModal.classList.add("image-modal");

        let closeButton = document.createElement("span");
        closeButton.classList.add("close");
        closeButton.innerHTML = "&times;";

        imageContainer.appendChild(imageModal);
        imageContainer.appendChild(closeButton);
        card.appendChild(imageElement);
        card.appendChild(imageContainer);

        // source type
        let typeContainer = document.createElement("div");
        typeContainer.classList.add("type-container");

        let typeElement = document.createElement("p");
        typeElement.innerHTML = "Type: " + data[i].type;

        let sourceTypeElement = document.createElement("div");
        sourceTypeElement.innerHTML = "Source: " + data[i].source_type;
        sourceTypeElement.classList.add("source-type");

        let sourceLinkElement = document.createElement("a");
        sourceLinkElement.href = data[i].source_link;
        sourceLinkElement.target = "_blank";
        sourceLinkElement.innerHTML = "Source: " + data[i].source_type;

        typeContainer.appendChild(typeElement);
        typeContainer.appendChild(sourceLinkElement);
        card.appendChild(typeContainer);

        // caption
        let captionElement = document.createElement("p");
        captionElement.innerHTML = data[i].caption;
        card.appendChild(captionElement);

        // user data
        let profileImage = document.createElement("img");
        profileImage.classList.add("profile-image");
        profileImage.setAttribute("src", data[i].profile_image);

        let container = document.createElement("div");
        container.classList.add("user-container");

        let innerContainer = document.createElement("div");
        innerContainer.classList.add("inner-container");

        let nameElement = document.createElement("p");
        nameElement.innerHTML = data[i].name + "<br>" + data[i].date;
        nameElement.classList.add("user-element");

        let likesElement = document.createElement("p");
        likesElement.classList.add("likes");
        likesElement.innerHTML = data[i].likes + " likes";

        innerContainer.appendChild(profileImage);
        innerContainer.appendChild(nameElement);

        container.appendChild(innerContainer);
        container.appendChild(likesElement);

        card.appendChild(container);

        cardsContainer.appendChild(card);

        // event listener for enlarging the images
        imageElement.addEventListener("click", function () {
          imageModal.src = this.src;
          imageContainer.style.display = "block";
        });

        closeButton.addEventListener("click", function () {
          imageContainer.style.display = "none";
        });

        if(i == endIndex){ 
          break 
        }
      }
      
      // scrolling to new cards on pc resolution
      const lastCard = cardsContainer.lastChild;
      const isMobileResolution = window.innerWidth <= 768;

      if (!isMobileResolution) {
        lastCard.scrollIntoView({ behavior: "smooth" });
      }

      // updating hash
      let newHash = Number(number) + count;
      location.hash = newHash;

      if (location.hash === "#" + data.length) {
        loadMoreBtn.remove();
      }
    }
  })
  .catch((error) => {
    alert("An error occurred. Please try again later.");
  });
